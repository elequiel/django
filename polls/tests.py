import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from polls.models import Question
from polls.models import Choice

class QuestionModelsTests(TestCase):

    def test_publicado_recentemente_com_questao_futura(self):
        """publicado_recentemente() retornará falso para questões que estiverem no futuro
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.publicado_recentemente(), False)

    def test_publicado_recentemente_com_questao_antiga(self):
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.publicado_recentemente(), False)

    def test_publicado_com_questao_recente(self):
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.publicado_recentemente(), True)


def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])


    def test_past_question(self):
        past_question = create_question(question_text='Past Question.', days=-5)
        url = reverse('polls:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text,)


    def test_future_question(self):
        future_question = create_question(question_text='Future Question', days=5)
        url = reverse('polls:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


    def test_future_question_and_past_question(self):
        create_question(question_text='Past question', days=-30)
        create_question(question_text='Future question', days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past question>']
        )

    def test_two_past_questions(self):
        create_question(question_text="Past question 1.", days=-30)
        create_question(question_text="Past question 2.", days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past question 2.>', '<Question: Past question 1.>']
        )

"""    def test_questao_sem_resposta(self):
        create_question(question_text='Questão aleatoria', days=0)
        resposta = Choice.choice_text=''
        response = self.client.get(reverse('polls:index'))
        self.assertIsNone(
            response.context['<question: Questao aleatoria', 'Não tem resposta']
        )
"""
